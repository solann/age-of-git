# Rules

Commit language :
  English

Commit message : "{TypeOfCommi}(FieldOfAction}): {subject of commit}"
Branch name : "{IssueNumber}-{P1/../P4}-{Action-of-feature-of-Branch}"

Name of Merge request = Name of Issue
No upper case

Link to the recap file :
https://docs.google.com/document/d/1Ee1AF3a1Io7utHsVgy-bjioI3ubJBLOJJG1QRMEDdec/edit#
