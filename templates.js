module.exports = {
  building: {
    CASTLE: {
      motif: ' C ',
      width: 6,
      height: 6
    },
    WATCHTOWER: {
      motif: ' G ',
      width: 1,
      height: 1
    },
    FORUM: {
      motif: ' + ',
      width: 3,
      height: 3
    },
    MILL: {
      motif: ' % ',
      width: 3,
      height: 2
    },
    WONDER: {
      motif: ' $ ',
      width: 10,
      height: 10
    },
    MARKET: {
      motif: ' M ',
      width: 3,
      height: 4
    },
    ARCHERY: {
      motif: ' A ',
      width: 5,
      height: 5
    },
    STABLE: {
      motif: ' S ',
      width: 5,
      height: 5
    },
    HOUSE: {
      motif: ' H ',
      width: 2,
      height: 2
    },
    FORGE: {
      motif: ' F ',
      width: 4,
      height: 3
    },
    UNIVERSITY: {
      motif: ' U ',
      width: 3,
      height: 4
    },
    PRISON: {
      motif: ' P ',
      width: 3,
      height: 3
    },
    BARRACK: {
      motif: ' X ',
      width: 5,
      height: 5
    },
    MONASTERY: {
      motif: ' @ ',
      width: 4,
      height: 4
    },
    TOWER: {
      motif: ' T ',
      width: 3,
      height: 3
    },
    FARM: {
      motif: '###',
      width: 4,
      height: 4
    },
    GATE_H: {
      model: ["[  ", "   ", "   ", "  ]"]
    },
    GATE_V: {
      model: ["MMM", "   ", "   ", "WWW"]
    }
  },
  wall: {
    HORIZONTAL: {
      SMALL: {
        motif: '---'
      },
      LARGE: {
        motif: '==='
      }
    },
    VERTICAL: {
      SMALL: {
        motif: ' | '
      },
      LARGE: {
        motif: '|||'
      }
    }
  }
};
